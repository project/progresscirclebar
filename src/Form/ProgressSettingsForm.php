<?php

namespace Drupal\progresscirclebar\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Progress Settings Form.
 */
class ProgressSettingsForm extends ConfigFormBase {
  use StringTranslationTrait;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a ProgressSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger) {
    parent::__construct($config_factory, 'progresscirclebar.settings'); // Pass the config name here.
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['progress.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_progresscirclebar_configuration';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $default_circle_wrapper_color = '#1A2C34';
    $default_circle_val_color = '#00ff00';
    $default_bar_wrapper_color = '#1A2C34';
    $default_bar_val_color = '#00ff00';
    $settings = $this->config('progress.settings');

    if (!empty($settings->get('progress_circle_wrapper'))) {
      $default_circle_wrapper_color = $settings->get('progress_circle_wrapper');
    }
    if (!empty($settings->get('progress_circle_value'))) {
      $default_circle_val_color = $settings->get('progress_circle_value');
    }
    if (!empty($settings->get('progress_bar_wrapper'))) {
      $default_bar_wrapper_color = $settings->get('progress_bar_wrapper');
    }
    if (!empty($settings->get('progress_bar_value'))) {
      $default_bar_val_color = $settings->get('progress_bar_value');
    }

    $form['progress']['progress_circle_wrapper'] = [
      '#title' => $this->t("Progress Circle Wrapper Color"),
      '#type' => 'color',
      '#default_value' => $default_circle_wrapper_color,
    ];

    $form['progress']['progress_circle_value'] = [
      '#title' => $this->t("Progress Circle Value Color"),
      '#type' => 'color',
      '#default_value' => $default_circle_val_color,
    ];

    $form['progress']['progress_bar_wrapper'] = [
      '#title' => $this->t("Progress Bar Wrapper Color"),
      '#type' => 'color',
      '#default_value' => $default_bar_wrapper_color,
    ];

    $form['progress']['progress_bar_value'] = [
      '#title' => $this->t("Progress Bar Value Color"),
      '#type' => 'color',
      '#default_value' => $default_bar_val_color,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $progress_circle_wrapper = $form_state->getValue('progress_circle_wrapper');
    $progress_circle_value = $form_state->getValue('progress_circle_value');
    $progress_bar_wrapper = $form_state->getValue('progress_bar_wrapper');
    $progress_bar_value = $form_state->getValue('progress_bar_value');

    // Load configuration object and save values.
    $config = $this->config('progress.settings');
    $config->set('progress_circle_wrapper', $progress_circle_wrapper)->save();
    $config->set('progress_circle_value', $progress_circle_value)->save();
    $config->set('progress_bar_wrapper', $progress_bar_wrapper)->save();
    $config->set('progress_bar_value', $progress_bar_value)->save();
    $this->messenger->addMessage($this->t('The settings have been saved!'));
  }

}
