<?php

namespace Drupal\progresscirclebar\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'field_progress_bar' formatter.
 *
 * @FieldFormatter(
 *   id = "field_progress_bar",
 *   module = "progresscirclebar",
 *   label = @Translation("Progress Bar"),
 *   field_types = {
 *     "string",
 *     "progresscirclebar"
 *   }
 * )
 */
class ProgressBarFormatter extends FormatterBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Constructs a new ProgressCircleFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    ConfigFactoryInterface $config_factory,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->config = $config_factory->get('progress.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $bar_val_color = '#00ff00';
    $bar_wrapper_color = '#1A2C34';
    if (!empty($this->config->get('progress_bar_wrapper'))) {
      $bar_wrapper_color = $this->config->get('progress_bar_wrapper');
    }
    if (!empty($this->config->get('progress_bar_value'))) {
      $bar_val_color = $this->config->get('progress_bar_value');
    }

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        'bar_value' => $item->value,
      ];
    }

    $build = [
      '#theme' => 'progressbar',
      '#progress_items' => $elements,
      '#attached' => [
        'drupalSettings' => [
          'bar_val_color' => $bar_val_color,
          'bar_wrapper_color' => $bar_wrapper_color,
        ],
      ],
    ];

    return $build;
  }

}
