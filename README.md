# Progress Circle/Bar

The Progress Circle/Bar Module provides simple way to add counter/poll field
to content type.
Based on selection it converts the percentage to a beautiful circular counter
or progress bar (whichever selected in configuration)
The Progress Circle/Bar module uses simple javascript due to that its very light
weight and doesn't effect performance.
The Progress Circle/Bar module provides configuration for choosing circular UI
or Bar UI and it also provides configuration to select background colors for
Circle/Bar.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. After enabling the Module.
1. Go to any of your Content type -> Manage field ->
   Add new field -> SelectProgress Circle/Bar.
   Then Go to Manage Display and change the
   Display setting for the field to Bar or Circle
   as per your need.

1. If you want to change the colors
   for Bar or Circle then Navigate
   to : `/admin/config/system/progressoptions`
   or Configuration -> System -> Progress Settings and
   add background colors for circle and bar
   as per your need.

## Maintainers

- Zeeshan khan - [zeeshan_khan](https://www.drupal.org/u/zeeshan_khan)
